(ns excercise1)
(def invoice (clojure.edn/read-string (slurp "invoice.edn")))
;(println invoice)

(defn findIva
  [taxe]
  (:tax/rate (get (:taxable/taxes taxe) 0))
  )
(defn findRate
  [taxe]
  (:retention/rate (get (:retentionable/retentions taxe) 0))
  )

(defn filterInvoices
  [invoice]
  (map (fn [item] (if (and (= (findIva item) 19) (= (findRate item) 1))
                    ""
                    (if (or (= (findIva item) 19) (= (findRate item) 1))
                      item
                      ""
                      )
                   )) (:invoice/items invoice))
  )

;(println (filterInvoices invoice))

(defn filterInvoices->>
  [invoice]
 (->> (:invoice/items invoice)
      (map (fn [item] (if (and (= (findIva item) 19) (= (findRate item) 1))
                   ""
                   (if (or (= (findIva item) 19) (= (findRate item) 1))
                     item
                     ""
                     )
                   ))))
  )

(println (filterInvoices->> invoice))